import React from 'react';

const AddPost = (props) => {
    return (
        <div className='mt-5'>
            <h1 className='mb-4'>{props.title}</h1>
            <form onSubmit={props.send}>
                <div className="input-group input-group-lg mb-5">
                    <span className="input-group-text" id="inputGroup-sizing-lg">Title</span>
                    <input onChange={props.inputChange} value={props.value} type="text" className="form-control" aria-label="Sizing example input"
                           aria-describedby="inputGroup-sizing-lg"/>
                </div>
                <div className="input-group mb-4">
                    <span className="input-group-text">Description</span>
                    <textarea onChange={props.areaChange} value={props.areaValue} className="form-control" style={{height: '200px'}} aria-label="Description"/>
                </div>
                <button  className='btn btn-primary btn-lg'>{props.btn}</button>
            </form>
        </div>
    );
};

export default AddPost;
import React, {useEffect, useState} from 'react';
import axiosPosts from "../../axiosPosts";


const PostId = (props) => {
    const [info, setInfo] = useState([])

    useEffect(() => {
        const res = async () => {
            const response = await axiosPosts.get('/posts/' + props.match.params.id + '.json');
            const data = response.data;
            setInfo(data);
        }

        res().catch(console.error);
    }, [props.match.params.id]);


    const change = () => {
        const params = new URLSearchParams(info);
        props.history.push(props.match.params.id + '/edit?' + params.toString());
    }

    const del = async () => {
        await axiosPosts.delete('/posts/' + props.match.params.id + '.json',);
        props.history.push('/');
    };


    return (
        <div className='border p-3 mt-5'>
            <p>Created at: {info.date}</p>
            <h2>{info.title}</h2>
            <strong>{info.body}</strong>
            <div className='btns mt-3'>
                <button className='btn btn-primary' onClick={change}>Edit</button>
                <button className='btn btn-danger' onClick={del}>Delete</button>
            </div>
        </div>
    );
};

export default PostId;
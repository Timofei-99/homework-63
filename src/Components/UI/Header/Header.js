import React from "react";
import './Header.css';
import {NavLink} from "react-router-dom";


const Header = () => {
    return (
        <header className="Header">
            <div className="Container">
                <ul>
                    <li>
                        <NavLink
                            exact
                            to="/"
                            activeStyle={{
                                fontWeight: "bold",
                                color: "white",
                            }}
                        >
                            Home
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            to="/add"
                            activeStyle={{
                                fontWeight: "bold",
                                color: "white",
                            }}
                        >
                            Add
                        </NavLink>
                    </li> <li>
                        <NavLink
                            to="/about"
                            activeStyle={{
                                fontWeight: "bold",
                                color: "white",
                            }}
                        >
                            About
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            to="/contacts"
                            activeStyle={{
                                fontWeight: "bold",
                                color: "white",
                            }}
                        >
                            Contacts
                        </NavLink>
                    </li>
                </ul>
            </div>
        </header>
    );
};

export default Header;

import React, {useState} from 'react';
import AddPost from "../../Components/AddPost/AddPost";
import axiosPosts from "../../axiosPosts";


const FormChange = ({location, history, match}) => {

    const parseState = () => {
        const params = new URLSearchParams(location.search);
        return Object.fromEntries(params);
    };

    const [post, setPost] = useState(parseState());

    const onChange = e => {
        setPost(prev => ({
            ...prev,
            title: e.target.value
        }));
    };

    const areaChange = e => {
        setPost(prev => ({
            ...prev,
            body: e.target.value
        }))
    };

    const PUT = async e => {
        e.preventDefault();
        await axiosPosts.put('/posts/' + match.params.id + '.json', post);
        history.push('/');
    };

    return (
        <div>
            <AddPost
                title='Change post'
                btn='Save'
                inputChange={onChange}
                areaChange={areaChange}
                value={post.title}
                areaValue={post.body}
                send={PUT}
            />
        </div>
    );
};

export default FormChange;
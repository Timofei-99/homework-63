import React, { useState} from 'react';
import './ContactsPage.css';

const ContactsPage = () => {

    const [info, setInfo] = useState({
        firstNumber: '+99999999',
        secondNumber: '+888888888',
        lastNumber: '+7777777777',
    });


    return (
        <div className='border mt-5 p-3 text-center'>
            <h1>По вопросам обращаться по номерам:</h1>
            <ul className='numbers'>
                <li>{info.firstNumber}</li>
                <li>{info.secondNumber}</li>
                <li>{info.lastNumber}</li>
            </ul>
        </div>
    );
};

export default ContactsPage;
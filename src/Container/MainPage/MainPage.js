import React, {useEffect, useState} from 'react';
import Post from "../../Components/Post/Post";
import axiosPosts from "../../axiosPosts";

const MainPage = ({history}) => {
    const [post, setPost] = useState({});

    useEffect(() => {
        const getPost = async () => {
            const response = await axiosPosts.get('/posts.json');
            const posts = response.data;
            setPost(posts)
        }

        getPost().catch(console.error);
    }, []);
    console.log(post);

    return (
        post ? <div className='d-flex flex-column pt-5'>
            {Object.keys(post).map(p => {

                const showMore =  (id) => {
                    if (p === id) {
                        history.push(p);
                    }
                };

                return (
                    <Post
                        key={p}
                        title={post[p].title}
                        date={post[p].date}
                        info={() => showMore(p)}
                    />
                )
            }).reverse()}
        </div> : <h1 className='text-center mt-5'>Nothing to show!</h1>
    );
};

export default MainPage;
import React, {useState} from 'react';
import AddPost from "../../Components/AddPost/AddPost";
import dayjs from "dayjs";
import axiosPosts from "../../axiosPosts";

const AddNewPost = ({history}) => {
    const [post, setPost] = useState({
        title: '',
        body: '',
        date: '',
    });

    const onChange = e => {
        setPost(prev => ({
            ...prev,
            title: e.target.value
        }));
    };

    const areaChange = e => {
        setPost(prev => ({
            ...prev,
            body: e.target.value
        }))
    };

    const addPost = async e => {
        e.preventDefault();
        const dat = dayjs().format('DD.MM.YYYY HH:mm:ss');
        const newPost = {
            title: post.title,
            body: post.body,
            date: dat,
        }

        setPost(newPost);
        await axiosPosts.post('/posts.json', newPost)
        console.log(post)
        history.push('/');
    };


    return (
        <>
            <AddPost
                inputChange={onChange}
                value={post.title}
                areaChange={areaChange}
                areaValue={post.body}
                send={addPost}
                title='Add new post'
                btn='Send'
            />
        </>
    );
};

export default AddNewPost;
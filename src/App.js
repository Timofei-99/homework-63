import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Header from "./Components/UI/Header/Header";
import AddNewPost from "./Container/AddNewPost/AddNewPost";
import MainPage from "./Container/MainPage/MainPage";
import PostId from "./Components/PostId/PostId";
import FormChange from "./Container/FormChange/FormChange";
import ContactsPage from "./Container/ContactsPage/ContactsPage";
import About from "./Container/About/About";

function App() {
    return (
        <BrowserRouter>
            <Header/>
            <div className="container">
                <Switch>
                    <Route path='/' exact component={MainPage}/>
                    <Route path='/add' component={AddNewPost}/>
                    <Route path='/contacts' component={ContactsPage}/>
                    <Route path='/about' component={About}/>
                    <Route path='/:id' exact component={PostId}/>
                    <Route path='/:id/edit' component={FormChange}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;

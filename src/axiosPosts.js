import axios from "axios";

const axiosPosts = axios.create({
    baseURL: 'https://blog-2-5ee48-default-rtdb.firebaseio.com/'
})

export default axiosPosts;